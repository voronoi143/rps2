
const choiceButtons = document.querySelectorAll('.choiceButton');

const playerScoreCount = document.getElementById("playerScoreCount");
const devilScoreCount = document.getElementById("devilScoreCount");
const gameMessage = document.getElementById("message");
const container = document.getElementById("container1");
const startButton = document.getElementById("start");
const intro = document.getElementById("introduction");
const playerChoiceCard = document.getElementById("player-choice-card");
const devilChoiceCard = document.getElementById("devil-choice-card");
const buttonsContainer = document.getElementsByClassName("buttons-container");

function getComputerChoice() {
    let x = (Math.random()*3)+1;
    let choice = Math.floor(x);
    
  

    if (choice==1) {
        return("Rock");
    } else if (choice==2){
        return("Paper");
    } else {
        return("Scissors")}
}



function playRound (playerSelection, computerSelection){
    playerSelection = playerSelection.toLowerCase();
    computerSelection = computerSelection.toLowerCase();

    displayDevilChoicePicture(computerSelection);

    // console.log(`You:${playerSelection}`);
    // console.log(`Computer:${computerSelection}`)


    switch(playerSelection===computerSelection) {
      case true:
        // console.log("CASE DRAW");
        gameMessage.textContent="Draw";
        beforeNewRound();
        break;
      case false:
        switch(playerSelection){
          case "rock":
            console.log("CASE ROCK");
            announceRoundWinner((computerSelection=="paper")?`You lose! ${computerSelection} beats ${playerSelection}`:`You won! ${playerSelection} beats ${computerSelection}`);
            break;
          case "paper":
            console.log("CASE PAPER");
            announceRoundWinner((computerSelection=="scissors")?`You lose! ${computerSelection} beats ${playerSelection}`:`You won! ${playerSelection} beats ${computerSelection}`);
            break;
          case "scissors":
            console.log("CASE SCISSORS");
            announceRoundWinner((computerSelection=="rock")?`You lose! ${computerSelection} beats ${playerSelection}`:`You won! ${playerSelection} beats ${computerSelection}`);
            break;
        }
    }
  }


function announceRoundWinner(roundResult){
    gameMessage.textContent=roundResult;
    // console.log(`ANNOUNCE WINNER ${roundResult}`);
    if (roundResult.includes("You lose!")){
      devilScoreCount.textContent++;      
    }
    else {
      playerScoreCount.textContent++;
    }
    checkGameWinner();
}


//check if devil or player got 5 points
//if none, invoke beforeNewRound()
function checkGameWinner(){
  if(devilScoreCount.textContent>=5){
    gameMessage.textContent="Oh no! The Devil got you!";
    // addRemoveMouseEvents("remove");
  }else if(playerScoreCount.textContent>=5){
    gameMessage.textContent="Congratulations! You've beaten the devil!"
    // addRemoveMouseEvents("remove");
  } else {
    beforeNewRound();
  }
}


const regex = /\*-picture/gi;

// function blockButtons(buttonStatus){

//   switch (buttonStatus){
//     case true: {
//       choiceButtons.forEach(button=> {
//         button.classList.add("disabled");
        
//     });
//     break;
//     }

//     case false: {
//       choiceButtons.forEach(button=> {
//         button.classList.remove("disabled");
//     });
//     break;
//     }

//NEW BLOCKBUTTONS
function blockButtons(trueFalse){
  
  switch (trueFalse){
        case true: {
          choiceButtons.forEach(button=> {
            button.classList.add("disabled");
            button.disabled= true;
            buttonsContainer[0].classList.add("disabled");
            
        });
        console.log("player buttons are BLOCKED");
        break;
        }
    
        case false: {
          choiceButtons.forEach(button=> {
            button.classList.remove("disabled");
            button.disabled= false;
            buttonsContainer[0].classList.remove("disabled");
        });
        console.log("player buttons are UNBLOCKED");
        break;
        }
  }
}


//OLD BLOCKBUTTONS
// function blockButtons(buttonStatus){

//   choiceButtons.forEach(button=> {
//     button.disabled = buttonStatus;
// })
// }

function bufferFunction(){
  //buttons are disabled for mouse events
  blockButtons(true);
  addRemoveMouseEvents("remove")


  let bufferPlayerChoice = this.getAttribute("data-choice");
  playRound(bufferPlayerChoice, getComputerChoice());
}


function beforeNewRound(){
  let strings = ["Rock", "Paper", "Scissors", "Make your choice"];
  let index = 0;
  let printAtInterval;

  // //remove mouse events
  // // addRemoveMouseEvents("remove");

  // //wait 1.25s and:
  // setTimeout(()=>{
  //   //1. wait 0.5s and invoke removePictures
  //   setTimeout(removePictures, 500);
  //   //2. wait 0.8s and make the pictures move
  //   setTimeout(movePictures, 800);

  //   //3. express and invoke printAtInterval which:
  //     // * calls interval which:
    
  //   setTimeout(()=>{
  //     printAtInterval = setInterval(()=>{
  //       // 1) every 0.6s prints a string corresponding to the current interval iteration
  //     gameMessage.textContent = strings[index];
  //     index++;
  //       // after every iteration checks if:
  //         //the array has been completely printed
  //     if (index>=strings.length){
            
  //       clearInterval(printAtInterval);
              
  //       setTimeout(blockButtons,100,false);
  //     }
  //   },600); 
  //   }, 200)

  // },1250);


  //wait for 1 seconds and start
  const initialTime = 1000;
  setTimeout(()=>{
    //remove choice pictures from cards and replace them with fists
    //remove after a short pause
    setTimeout(removePictures, initialTime);
    

    //make the fists move 
    //make the fists move after 1 second since the picteres are removed
    setTimeout(shakeFists, initialTime+750);

    //print strings
    //strings print needs to happen in tune with the fist shake
    setTimeout(()=>{
      //outputs a string, at an interval 600ms
      printAtInterval = setInterval(()=>{
        console.log(index);
        gameMessage.textContent = strings[index];
        index++;

        //if you've reached the end of the string
        //stop the interval
        //
        if(index>=strings.length){
          console.log("stop print");
          clearInterval(printAtInterval);
          setTimeout(addRemoveMouseEvents, 100, "add");
          //after the last message has been printed, wait 0.1s and enable buttons
          setTimeout(blockButtons,100,false);
        }
      }, 600)
    },initialTime)

  }, 0)

}



  

function shakeFists(){
  playerChoiceCard.classList.add("fist-picture-move-player");
  devilChoiceCard.classList.add("fist-picture-move-devil");
  console.log("fists shaked");
}

//removes all classes (classes are used to add pictures)
//replaces them with "fist-picture" class
//
function removePictures() {
  playerChoiceCard.removeAttribute('class');
  playerChoiceCard.classList.add("fist-picture");
  // playerChoiceCard.classList.add("fist-picture-move-player");
  
  devilChoiceCard.removeAttribute('class');
  devilChoiceCard.classList.add("fist-picture-inverted");
  // devilChoiceCard.classList.add("fist-picture-move-devil");
  console.log("pictures removed");
}


function addChoicePicture () {
  console.log(this);
  playerChoiceCard.className="";
  playerChoiceCard.classList.add(`${this.getAttribute("data-choice")}-picture`);
  console.log("Player picture added");
}



function removeChoicePicture () {
  console.log(this);
  playerChoiceCard.className="";
  playerChoiceCard.classList.add("fist-picture");
  console.log("Player picture set to fist");
}


function displayDevilChoicePicture (devilChoice){
    devilChoiceCard.className="";
    devilChoiceCard.classList.add(`${devilChoice}-picture`)
}

//BEGIN THE GAME
startButton.addEventListener('click', startGame);

function startGame(){
  //hide the intro screen
  intro.style.display="none";
  container.style.display="flex";
  blockButtons(true);
  //prepare for the new round
  
  
  //add event listeners
  choiceButtons.forEach(button => {
    
  //add event listener to every button. The listener invokes bufferFunction
  button.addEventListener('click', bufferFunction);
  button.addEventListener("mouseover", addChoicePicture);
  button.addEventListener("mouseout", removeChoicePicture);
});

beforeNewRound();
}


function addRemoveMouseEvents(string){
  switch(string) {
    case "remove":{
      choiceButtons.forEach(button => {
        button.removeEventListener("mouseover", addChoicePicture);
        button.removeEventListener("mouseout", removeChoicePicture);
      });
      break;
    }
    case "add":{
      choiceButtons.forEach(button => {
        button.addEventListener("mouseover", addChoicePicture);
        button.addEventListener("mouseout", removeChoicePicture);
      });
      break;
  }
  } 
  
}




